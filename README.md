-------------------------
General information
-------------------------

Type of work:   Project

University:        University of Trento

Department:     Department of Engineering and Computer Science

Subject:            Computer Vision

Title:                  Ground Floor Estimation and Obstacles Detection with Microsoft Kinect

------------------------------------------------------
Point Cloud Library Installation for Ubuntu
------------------------------------------------------ 

Source:              http://pointclouds.org/downloads/linux.html

The installation instructions are:

    sudo add-apt-repository ppa:v-launchpad-jochen-sprickerhof-de/pcl
    sudo apt-get update
    sudo apt-get install libpcl-all

------------------------------------------
Building and Running the project
------------------------------------------

1. After PCL installation we should create a folder (ex. /home/project).
2. Copy CMakeLists.txt and pcdv_gfenod.cpp to the choosen folder.
3. In console (ctrl + alt + t) go to the choosen folder. (ex. cd /home/project)
4. Next commands are:
   ```
        mkdir build
        cd build
        cmake ..
        make
   ```
5. To run the project write
        ./pcdv_gfenod

The file should be in build directory.

------------------------
Possible problems
------------------------

If the window appears but suddenly fade away, then there could be problems with XnSensorServer.


The text of the error could be:
--      
    pcdv_gfenod: /usr/include/boost/smart_ptr/shared_ptr.hpp:412: boost::shared_ptr<T>::reference boost::shared_ptr<T>::operator*() const [with T = xn::NodeInfo, boost::shared_ptr<T>::reference = xn::NodeInfo&]: Assertion `px != 0' failed.
    Aborted (core dumped)
--

Solution:

1. In bash write
        killall XnSensorServer
        XnSensorServer
2. Press CTRL + C to stop running server.
Sometimes this operation doesn't help with the first try.
