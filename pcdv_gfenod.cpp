#include <iostream>
#include <math.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <pcl/point_cloud.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/io/openni_camera/openni_driver.h>
#include <pcl/console/parse.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/common/time.h>
#include <pcl/common/common.h>
#include <pcl/filters/passthrough.h>

 //Type define
typedef pcl::PointXYZ CloudType;
typedef pcl::PointCloud<CloudType> Cloud;
typedef pcl::PointCloud<CloudType>::Ptr CloudPtr;
typedef pcl::PointCloud<CloudType>::ConstPtr CloudConstPtr;
boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("Finestra"));

pcl::SACSegmentation<pcl::PointXYZ> seg;
pcl::ExtractIndices<pcl::PointXYZ> extract;

//For rotation
double theta = -0.4363f; // -25 degrees
const float cosTheta=cos(theta);
const float sinTheta=sin(theta);

struct st1
{
  CloudConstPtr cloud_r; // cloud received
  boost::mutex cloud_mutex;
  
  void cloud_callback(const CloudConstPtr& cloud)
  {
    boost::mutex::scoped_lock lock (cloud_mutex);
    cloud_r = cloud;
  }
  
  CloudConstPtr cloud_process()
  {
    boost::mutex::scoped_lock lock(cloud_mutex);
// ====== Initialization ==========================
  sensor_msgs::PointCloud2::Ptr cloud_blob (new sensor_msgs::PointCloud2);
  sensor_msgs::PointCloud2::Ptr cloud_filtered_blob (new sensor_msgs::PointCloud2);
  
  CloudPtr cloud_filtered (new Cloud);
  CloudPtr cloud_p (new Cloud);
  CloudPtr cloud_f (new Cloud);

  CloudPtr cloud_rotated (new Cloud);
  
  // Clear the view window
  viewer->setBackgroundColor(0,0,0);
  viewer->removeAllPointClouds();
// ====== Downsampling ============================

  pcl::toROSMsg (*cloud_r, *cloud_blob);
  //   Create the filtering object: downsample the dataset using a leaf size of 1cm
  pcl::VoxelGrid<sensor_msgs::PointCloud2> sor;
  sor.setInputCloud (cloud_blob);
  sor.setLeafSize (0.02f, 0.02f, 0.02f);
  sor.filter (*cloud_filtered_blob);

  // Convert to the templated PointCloud
  pcl::fromROSMsg (*cloud_filtered_blob, *cloud_filtered);
    
  
  
// ====== PassThroughFilter ==========================
    
//   Create filtering object
  pcl::PassThrough<CloudType> pass;
  pass.setInputCloud(cloud_filtered);
  pass.setFilterFieldName("z");
  pass.setFilterLimits(0.0,2.0);
  pass.filter(*cloud_filtered);
  

// ================= Rotation ==========================
  for (Cloud::const_iterator it = cloud_filtered->begin(); it !=cloud_filtered->end(); it++)
  {
    float x = it->x;
    float y = (it->y)*cosTheta - (it->z)*sinTheta;
    float z = (it->y)*sinTheta +(it->z)*cosTheta;
    y=(y-0.4f);
    CloudType point;
    point.x = x;
    point.y = -y;
    point.z = -z;
   cloud_rotated->push_back(point); 
  }
  

// ====== Segmentation ==========================
  
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
  
 
  
// ====== Extraction ==========================
 
  CloudType maxPlane(0,0,0);
  CloudType minPlane(0,0,0);
  CloudPtr floor(new Cloud);
  
  int i = 0, nr_points = (int) cloud_rotated->points.size ();
  // While 30% of the original cloud is still there
  while (cloud_rotated->points.size () > 0.3 * nr_points)
  {
    // Segment the largest planar component from the remaining cloud
    seg.setInputCloud (cloud_rotated);
    seg.segment (*inliers, *coefficients);
    if (inliers->indices.size () == 0)
    {
      std::cout<< "Could not estimate a planar model for the given dataset." << std::endl;
      break;
    }

    // Extract the inliers
    extract.setInputCloud (cloud_rotated);
    extract.setIndices (inliers);
    extract.setNegative (false);
    extract.filter (*cloud_p);
    
    // Check if it's an orthogonal
    pcl::getMinMax3D(*cloud_p, minPlane,maxPlane);
    if (floor->empty() && (maxPlane.y < 0.0))
    {
      // This is the floor
       *floor = *cloud_p;
    }
    else
    {
      // This is an obstacle
      pcl::visualization::PointCloudColorHandlerCustom<CloudType> red_col(cloud_p,255,0,0);
      viewer->addPointCloud<CloudType>(cloud_p,  red_col, "Obstacle"+i);
      viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "Obstacle"+i);
    }
    
    // Create the filtering object (outliers)
    extract.setNegative (true);
    extract.filter (*cloud_f);
    cloud_rotated.swap (cloud_f);
    i++;
  }
  
  
// ======= Visualization ================

    pcl::visualization::PointCloudColorHandlerCustom<CloudType> col(floor,0,255,0);
    viewer->addPointCloud<CloudType>(floor, col, "Floor");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "Floor");

    pcl::visualization::PointCloudColorHandlerCustom<CloudType> red_col(cloud_rotated,255,0,0);
    viewer->addPointCloud<CloudType>(cloud_rotated,  red_col, "Outliers");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "Outliers");
    
    viewer->spinOnce ();
    viewer->initCameraParameters();
    return (cloud_r);
  }
};

 int main ()
 {
   pcl::Grabber* interface = new pcl::OpenNIGrabber();
   st1 s;
   boost::function<void (const pcl::PointCloud<pcl::PointXYZ>::ConstPtr&)> f =
   boost::bind (&st1::cloud_callback, &s, _1);

  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);		
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setMaxIterations (1000);
  seg.setDistanceThreshold (0.01);
  
  
  
  interface->registerCallback (f);

  interface->start ();

  while (!viewer->wasStopped())
  {
    if(s.cloud_r)
    s.cloud_process();
  }

  interface->stop ();


   return 0;
 }
